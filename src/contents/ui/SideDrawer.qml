/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

Kirigami.GlobalDrawer {
	width: 300
	edge: Qt.LeftEdge
	topPadding: 0
	leftPadding: 0
	bottomPadding: 0
	rightPadding: 0

	// Dynamically collapse the drawer
	modal: !root.wideScreen || !enabled
	onModalChanged: drawerOpen = !modal
	onEnabledChanged: drawerOpen = enabled && !modal

	// Header bar
	header: Kirigami.AbstractApplicationHeader {
		contentItem: RowLayout {
			anchors.fill: parent

			// Search bar
			Kirigami.SearchField {
				Layout.fillWidth: true
				Layout.leftMargin: 2
				Layout.preferredHeight: filterButton.height - 2  // HACK
				focusSequence: StandardKey.Search
				placeholderText: !filterButton.checked ? i18n("Search in all lists...")
													   : i18n("Search in %1...", taskListModel.get(taskList.currentIndex).name)
			}

			// Filter button
			Controls.ToolButton {
				id: filterButton
				Layout.rightMargin: 6
				icon.name: "view-filter"
				checkable: true
				// TODO: Add a tooltip
			}
		}
	}

	// Task list
	//contentItem:
	Kirigami.ScrollablePage {
		Layout.fillHeight: true
		Layout.topMargin: -6  // HACK: Caused by putting ScrollablePage in GlobalDrawer

		ListModel {
			id: taskListModel
			ListElement { name: "Shopping" }
			ListElement { name: "School" }
		}

		ListView {
			id: taskList
			model: taskListModel
			currentIndex: 0

			delegate: Kirigami.BasicListItem {
				label: name
			}

			// New list button
			footer: Controls.ToolButton {
				anchors.left: parent.left
				anchors.right: parent.right
				text: i18n("Add a new list")
				onClicked: {
					// Append new list
					taskListModel.append({"name": i18n("New list")})
				}
			}
		}
	}
}
