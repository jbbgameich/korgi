/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

Kirigami.OverlaySheet {
	// Header
	header: Kirigami.Heading {
		text: i18n("About")
	}

	// Body
	ColumnLayout {
		Layout.preferredWidth: 300
		Controls.Label {
			Layout.fillWidth: true
			horizontalAlignment: Text.AlignHCenter
			textFormat: Text.StyledText
			wrapMode: Text.WordWrap
			text: i18n('<h2>Korgi</h2>A simple task list manager<br>Version 0.0.0<br><br><img src="qrc:/logo.svg" align="middle" width="70" height="70"><br>')
		}
		Controls.Label {
			Layout.fillWidth: true
			horizontalAlignment: Text.AlignJustify
			textFormat: Text.StyledText
			wrapMode: Text.WordWrap
			text: i18n('<b>Copyright © 2021 Ignacy Kajdan</b><br>This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.')
		}
	}
}
