/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami

Kirigami.ApplicationWindow {
	id: root
	title: i18n("Korgi")
	minimumWidth: 400
	minimumHeight: 500

	pageStack.initialPage: MainPage {
		id: mainPage
	}

	globalDrawer: SideDrawer {
		id: sideDrawer
	}
}
