/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

Kirigami.OverlaySheet {
	id: addTaskSheet
	onSheetOpenChanged: mainPage.actions.main.checked = sheetOpen;

	// Header
	header: Kirigami.Heading {
		text: i18n("New task")
	}

	// Body
	ColumnLayout {
		Layout.preferredWidth: 300

		// Title field
		Controls.TextField {
			id: title
			Layout.fillWidth: true
			placeholderText: i18n("Title")
		}

		// Description field
		Controls.TextArea {
			id: description
			Layout.fillWidth: true
			Layout.minimumHeight: Kirigami.Units.gridUnit * 5
			placeholderText: i18n("Description")
			wrapMode: Controls.TextArea.Wrap
		}

		// Priority selector
		Controls.GroupBox {
			id: priority
			title: "Priority"
			Layout.fillWidth: true
			Layout.alignment: Qt.AlignHCenter
			Layout.margins: 1  // HACK

			RowLayout {
				Controls.RadioButton {
					text: i18n("Low")
				}
				Controls.RadioButton {
					text: i18n("Normal")
					checked: true
				}
				Controls.RadioButton {
					text: i18n("Medium")
				}
				Controls.RadioButton {
					text: i18n("High")
				}
			}
		}
	}

	// Footer
	footer: Controls.Button {
		text: i18n("Add")
		icon.name: "list-add"
		onClicked: {
			// Append data FIXME: Any way to set default value from the model?
			listModel.append({"title": title.text,
							  "description": description.text,
							  "priority": priority,
							  "done": false})
			// Clean the fields
			title.remove(0, title.length)
			description.remove(0, description.length)
			// Close the sheet
			addTaskSheet.close();
		}
	}
}
