/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

Kirigami.ScrollablePage {
	id: mainPage
	title: i18n("Tasks")

	// Include AddTaskSheet
	AddTaskSheet {
		id: addTaskSheet
	}

	// Include AboutSheet
	AboutSheet {
		id: aboutSheet
	}

	// Include TaskDetailsSheet
	TaskDetailsSheet {
		id: taskDetailsSheet
	}

	// Toolbar action buttons
	actions {
		main: Kirigami.Action {
			icon.name: "list-add"
			text: root.wideScreen ? i18n("Add new task") : ""
			tooltip: i18n("Add a new task to the current list")
			shortcut: StandardKey.New
			checkable: true
			onCheckedChanged: addTaskSheet.sheetOpen = checked;
		}
		contextualActions: [
			Kirigami.Action {
				icon.name: "view-sort-descending-symbolic"
				text: i18n("&Sort by")
				tooltip: i18n("Change the sorting order")
				shortcut: "Ctrl+O"
				displayHint: Kirigami.Action.DisplayHint.AlwaysHide
				Kirigami.Action {
					icon.name: "sort-name"
					text: i18n("&Name")
				}
				Kirigami.Action {
					icon.name: "emblem-important-symbolic"
					text: i18n("&Priority")
				}
				Kirigami.Action {
					icon.name: "adjustrow"
					text: i18n("&Custom")
				}
			},
			Kirigami.Action {
				icon.name: "view-hidden"
				text: i18n("Show &done")
				tooltip: i18n("View tasks marked as done")
				shortcut: "Ctrl+H"
				checkable: true
				checked: true
				displayHint: Kirigami.Action.DisplayHint.AlwaysHide
			},
			Kirigami.Action {
				icon.name: "help-hint"
				text: i18n("&About")
				tooltip: i18n("Show information about Korgi")
				displayHint: Kirigami.Action.DisplayHint.AlwaysHide
				onTriggered: aboutSheet.open()
			}
		]
	}

	// Show greeter when the task list is empty
	Kirigami.PlaceholderMessage {
		anchors.centerIn: parent
		visible: taskList.count === 0
		icon.source: "qrc:/logo.svg"
		text: i18n("Your task list is empty")
		// 			helpfulAction: Kirigami.Action {
		// 				icon.name: "list-add"
		// 				text: i18n("Add a new task")
		// 			}
	}

	// Task list
	Component {
		id: delegateComponent
		Kirigami.SwipeListItem {
			id: listItem
			contentItem: RowLayout {
				MouseArea {
					anchors.fill: parent
					onClicked: {
						taskDetailsSheet.taskTitle = model.title
						taskDetailsSheet.taskDescription = model.description
						taskDetailsSheet.open()
					}
				}
				Kirigami.ListItemDragHandle {
					listItem: listItem
					listView: taskList
					onMoveRequested: listModel.move(oldIndex, newIndex, 1)
				}
				Controls.Label {
					Layout.fillWidth: true
					font.strikeout: model.done
					text: model.title
				}
			}

			// Task actions
			actions: [
				Kirigami.Action {
					iconName: "delete"
					text: i18n("Delete")
					visible: done
					onTriggered: showPassiveNotification(i18n("The task has been deleted."), 5000, i18n("Undo"))
				},
				Kirigami.Action {
					iconName: done ? "edit-undo-symbolic" : "checkbox"
					text: done ? i18n("Restore") : i18n("Mark as done")
					onTriggered: model.done = !model.done
				}
			]
		}
	}

	ListModel {
		id: listModel
	}

	ListView {
		id: taskList
		model: listModel
		currentIndex: -1  // Don't highlight any entry

		// Reordering animation
		moveDisplaced: Transition {
			YAnimator {
				duration: Kirigami.Units.longDuration
				easing.type: Easing.InOutQuad
			}
		}
		delegate: Kirigami.DelegateRecycler {
			width: parent.width
			sourceComponent: delegateComponent
		}
	}
}
