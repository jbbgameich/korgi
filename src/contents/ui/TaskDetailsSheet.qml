/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

Kirigami.OverlaySheet {
	id: taskDetailsSheet

	property string taskTitle: ""
	property string taskDescription: ""

	// Header
	header: Kirigami.Heading {
		text: taskTitle
	}

	// Body
	ColumnLayout {
		Layout.preferredWidth: 300
		Controls.Label {
			Layout.fillWidth: true
			wrapMode: Text.WordWrap
			text: taskDescription
		}
	}

	// Footer
	footer: Controls.Button {
		text: i18n("Edit")
		icon.name: "edit-entry"
		onClicked: {
			taskDetailsSheet.close();
		}
	}
}
