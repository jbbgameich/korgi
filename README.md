# Korgi

# >>> Work in progress <<<

<!-- <center><img src="data/assets/korgi.svgz" alt="Korgi logo" width="80"/></center> -->

A simple program that allows the user to easily organize their tasks.

Features:

- Simple and easy to navigate
- Should be convergent (haven't tested it yet)
- _TODO_

## Gallery

_TODO_

<!--<center>
    <img src="data/media/wide.png" alt="Promo image 1" height="300"/>
    <img src="data/media/narrow.png" alt="Promo image 2" height="300"/>
</center>-->

## Installation

1. Clone this repository

    `git clone _TODO_`

1. Install required dependencies (Fedora)

    `sudo dnf install _TODO_`

1. Build

    `cmake -B build/ .`
    `cmake --build build/`

1. Install

    `sudo cmake --install build/`

## License

    Korgi – simple task list manager
    Copyright © 2021 Ignacy Kajdan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
